from django.apps import AppConfig


class CoreTrackerConfig(AppConfig):
    name = 'core_tracker'
