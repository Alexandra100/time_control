from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Team(models.Model):
    id = models.AutoField(primary_key=True)

class Profile(models.Model):
    objects = models.Manager()
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.SET_DEFAULT, default=0)

    def __str__(self):
        return self.user.name

class ProgramProfile(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    time = models.IntegerField(default=0)
    timeLimit = models.IntegerField(default=-1)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Program(models.Model):
    id = models.AutoField(primary_key=True)
    profile = models.ForeignKey(ProgramProfile, on_delete=models.SET_DEFAULT, default=0)
    name = models.CharField(max_length=20)
    time = models.IntegerField()

    def __str__(self):
        return self.name

class HandbookProgram(models.Model):
    id = models.AutoField(primary_key=True)
