from django.conf.urls import url
from django.urls import include, path

from . import views
from . import api

apis = [
    path('addtasks/', api.addTask),
    path('removetasks/', api.removeTask),
]

urlpatterns = [
    path('', views.welcome_screen, name='welcome'),
    path('registration/', views.signup, name='signup'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('tasks/', views.tasks, name='tasks'),
    path('team/', views.team, name='team'),
    url('api/', include(apis)),
]