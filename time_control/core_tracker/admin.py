from django.contrib import admin

from .models import User, Profile, Program, Team, ProgramProfile

admin.site.register(Team)
admin.site.register(Profile)
admin.site.register(ProgramProfile)
admin.site.register(Program)