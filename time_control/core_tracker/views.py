from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from .forms import UserForm, TaskForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from .models import ProgramProfile, Program, Team, Profile
from django.contrib.auth.models import User

def welcome_screen(request):
    return render(
        request,
        'welcome_screen.html',
        context={},
    )

def signup(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('login')
            password = form.cleaned_data.get('password')
            beUser = User.objects.filter(username=username)
            if beUser.count() == 0:
                user = User.objects.create_user(username=username, password = password)
                user.save()
                user = auth.authenticate(username=username, password=password)
                auth.login(request, user)
                return redirect("/tasks")
            else:
                return render(request, 'registration/registration.html', {'form': form, "error": "Пользователь уже существует."})
    else:
        form = UserForm()
    return render(request, 'registration/registration.html', {'form': form, "error": ""})

#The function don't work.
def login(request):
    if request.method == 'POST':
        form = UserForm()
        data = request.POST.copy()
        errors = form.get_validation_errors(data)
        if not errors:
            username = form.cleaned_data.get('login')
            password = form.cleaned_data.get('password')
            user = auth.authenticate(username=username, password=password)
            if user is not None and user.is_active:
                auth.login(request, user)
                return redirect("/user_profile/")
            else:
                return redirect("registration/")
    return redirect("/registration/")

def logout(request):
    auth.logout(request)
    return redirect("/")

def tasks(request):
    if request.user.is_authenticated:
        tasklist = ProgramProfile.objects.all()
        return render(request,'tasks.html', {'tasklist': tasklist})
    else:
        return redirect("/registration/")

def team(request):
    if request.user.is_authenticated:
        user = Profile.objects.filter(user=request.user)
        tasks = []
        if (user is not None) and (user.count() != 0):
            team = user.team  
            tasks = team.programProfile_set.all()
        return render(request,'team.html', {'tasks' : tasks})
    else:
        return redirect("/registration/")