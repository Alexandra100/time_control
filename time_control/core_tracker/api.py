from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import auth
from django.contrib.auth.models import User 
from .models import ProgramProfile, Program, Team

#This api isn't tested.
def login(request):
    username = request.POST['login']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponse(200)
    else:
        return HttpResponse(400)

#This api isn't tested.:
def logout(request):
    auth.logout(request)
    return HttpResponse(200)

def addTask(request):
    name = request.GET.get("name", "")
    if name != "":
        programProfile = ProgramProfile()
        programProfile.owner = request.user
        programProfile.name = name
        programProfile.time = 0
        programProfile.timeLimit = 0
        programProfile.save()
        return HttpResponse(200)
    else:
        return HttpResponse(400)

def removeTask(request):
    programProfileId = request.GET.get("id", -1)
    if programProfileId != -1:
        programProfile = ProgramProfile.objects.get(id=programProfileId)
        programProfile.delete()
        return HttpResponse(200)
    else:
        return HttpResponse(404)