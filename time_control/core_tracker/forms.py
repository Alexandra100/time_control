from django import forms

class UserForm(forms.Form):
    login = forms.CharField(label="Логин", widget=forms.TextInput(attrs={"class":"input-xlarge"}))
    password = forms.CharField(label="Пароль", min_length=8, max_length=20, widget=forms.PasswordInput(attrs={"class":"input-xlarge"}))
    password2 = forms.CharField(label="Повторите пароль", min_length=8, max_length=20, widget=forms.PasswordInput(attrs={"class":"input-xlarge"}))

class UserFormForLogin(forms.Form):
    login = forms.CharField(label="Логин", widget=forms.TextInput(attrs={"class":"input-xlarge"}))
    password = forms.CharField(label="Пароль", min_length=8, max_length=20, widget=forms.PasswordInput(attrs={"class":"input-xlarge"}))

class TaskForm(forms.Form):
    name = forms.CharField(label="Название задачи", widget=forms.TextInput(attrs={"class":"input-xlarge"}))